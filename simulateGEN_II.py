#!/usr/bin/env python
#--->>> simulateGEN__V2b__.py
# testing for gnu parallel
import os, subprocess, sys, math, random
import numpy as np

# phaseFile = "phase.txt"
work_DIR = sys.argv[1]
phaseFile = sys.argv[2]
varPerScaf = int(sys.argv[3])
freqFile = sys.argv[4]
outputFilenameID = sys.argv[5]
os.chdir(work_DIR)

#######################
#	WITH LINKAGE: ---> same LD (r^2) for all adjacent loci within scaffolds
#######################
# Pab_max = min(Pa, Pb)/max(Pa, Pb)
# r2_max = (Pab - PaPb)^2 / PaPb(1-Pa)(1-Pb)

phase = np.genfromtxt(phaseFile, delimiter='\t', dtype=int)
nLOCI = phase.shape[0]
freqREF = np.genfromtxt(freqFile, delimiter='\t', usecols=1, dtype=float)
freqALT = 1 - freqREF

GEN=[]
for i in range(nLOCI):
	if (i+1)%varPerScaf==1:
		a1 = np.random.choice([0, 1], p=(freqREF[i], freqALT[i]))	#No LD between scaffolds
		a2 = np.random.choice([0, 1], p=(freqREF[i], freqALT[i]))
	else:
		if phase[i-1]==phase[i]:
			if (phase[i-1]==0 and a1==0) or (phase[i-1]==1 and a1==0):
				Pab = min(freqREF[i-1], freqREF[i]) / max(freqREF[i-1], freqREF[i])
				a1 = np.random.choice([0, 1], p=(Pab, 1-Pab))
				a2 = np.random.choice([0, 1], p=(Pab, 1-Pab))
			else:
				Pab = min(freqALT[i-1], freqALT[i]) / max(freqALT[i-1], freqALT[i])
				a1 = np.random.choice([1, 0], p=(Pab, 1-Pab))
				a2 = np.random.choice([1, 0], p=(Pab, 1-Pab))
		else:
			if (phase[i-1]==0 and a1==0) or (phase[i-1]==1 and a1==0):
				Pab = min(freqREF[i-1], freqALT[i]) / max(freqREF[i-1], freqALT[i])
				a1 = np.random.choice([1, 0], p=(Pab, 1-Pab))
				a2 = np.random.choice([1, 0], p=(Pab, 1-Pab))
			else:
				Pab = min(freqALT[i-1], freqREF[i]) / max(freqALT[i-1], freqREF[i])
				a1 = np.random.choice([0, 1], p=(Pab, 1-Pab))
				a2 = np.random.choice([0, 1], p=(Pab, 1-Pab))
	GEN.append(str(a1) + "|" + str(a2))

GEN_OUT = np.concatenate(([outputFilenameID], GEN), axis=0)
np.savetxt(outputFilenameID, GEN_OUT, fmt='%s' ,delimiter="\t")

#for testing:
# work_DIR = "/mnt/SIMULATED/DNA"
# phaseFile = "phase.txt"
# varPerScaf = 20
# freqFile = "Simulated_Lolium_perenne_GENOTYPES.freq"
# outputFilenameID = "GENO1"