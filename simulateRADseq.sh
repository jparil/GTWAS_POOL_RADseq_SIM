#!/bin/bash
DIR=$1
MINFRAG=$2
MAXFRAG=$3
INSLEN=$4
MUTPROB=$5
LOCNUM=$6
READNUM=$7
PREFIX=$8
KEEP_DEPTH=$9

# #for testing:
# DIR=/mnt/SIMULATED/DNA
# MINFRAG=50
# MAXFRAG=300
# INSLEN=75
# MUTPROB=0.80
# LOCNUM=200000
# READNUM=10000000
# PREFIX=TEST_INIT
# enzyme1=EcoRI
# enzyme2=MseI

mem=$(($(($(grep MemTotal /proc/meminfo | awk '{print $2}') / 1000000))-5))
thr=$(($(grep -c ^processor /proc/cpuinfo)-1))

cd ~/ddRADseqTools/Package*/
for k in $(seq $(cat Renzyme.txt | wc -l))
do
enzyme1=$(head -n$k Renzyme.txt | tail -n 1)

for j in $(seq $((k+1)) $(cat Renzyme.txt | wc -l))
do
enzyme2=$(head -n$j Renzyme.txt | tail -n 1)

#simulate RE digetion
./rsitesearch.py \
--genfile=${DIR}/Reference.fasta \
--enzyme1=$enzyme1 --enzyme2=$enzyme2 \
--minfragsize=$MINFRAG --maxfragsize=$MAXFRAG \
--fragstfile=${DIR}/${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-REDIGEST.txt \
--fragsfile=${DIR}/${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-FRAGMENTS.fasta &> /dev/null

#simulate sequencing
./simddradseq.py \
--fragsfile=${DIR}/${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-FRAGMENTS.fasta \
--individualsfile=poolsIND.txt \
--technique=IND1_IND2_DBR \
--enzyme1=$enzyme1 --enzyme2=$enzyme2 \
--readtype=PE \
--mutprob=$MUTPROB \
--insertlen=$INSLEN \
--locinum=$LOCNUM \
--readsnum=$READNUM \
--format=FASTQ \
--readsfile=${DIR}/${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-ILLREADS &> /dev/null
rm ${DIR}/${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-FRAGMENTS*

#trim-off the ends (the ends include the adapters, degenerate base regions, indices and primers)
./readstrim.py \
--readsfile1=${DIR}/${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-ILLREADS-1.fastq \
--readsfile2=${DIR}/${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-ILLREADS-2.fastq \
--technique=IND1_IND2_DBR \
--readtype=PE \
--format=FASTQ \
--trimfile=${DIR}/${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-TRIMMED &> /dev/null
rm ${DIR}/${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-ILLREADS*

#variant calling with bcftools mpileup .. | bcftools call ..
cd $DIR
#align, compress, and  sort
bwa mem -M Reference_genome \
${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-TRIMMED-1.fastq \
${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-TRIMMED-2.fastq | \
samtools view -Sb | samtools sort > ${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-SORTED.bam
rm ${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-TRIMMED-*

#extract coverage from sorted bam file
samtools depth -aa ${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-SORTED.bam \
> ${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-DEPTH.csv
rm ${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-SORTED.bam

#summarize
TOT_BASES=$(wc -l ${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-DEPTH.csv | cut -d$' ' -f1)
TOT_DEPTH=$(awk '{ total += $3 } END {print total}' ${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-DEPTH.csv)
TOTSQ_DEPTH=$(awk '{ total += $3^2 } END {printf "%.0f", total}' ${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-DEPTH.csv)
TOT_COVER=$(awk ' $3 > 0 { total++ } END {print total}' ${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-DEPTH.csv)
if [ $KEEP_DEPTH == 0 ]
then
rm ${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-DEPTH.csv ${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-REDIGEST*
fi

#OUTPUT TO SUMMARY FILE ${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-OUT.txt
# <REP> 	<MIN_FRAG> 	<MAX_FRAG> 	<ENZYME_1> 	<ENZYME_2> 	<TOTAL_BASES> <TOTAL_READS> <SUM_SQUARED_READS>
echo -e "$PREFIX\t$enzyme1\t$enzyme2\t$MINFRAG\t$MAXFRAG\t$INSLEN\t$READNUM\t$TOT_BASES\t$TOT_DEPTH\t$TOTSQ_DEPTH\t$TOT_COVER" \
> ${DIR}/${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-OUT.txt

#clean up
cd ~/ddRADseqTools/Package*/
done
done