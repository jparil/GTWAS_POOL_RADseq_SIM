#!/usr/bin/env python
import os, subprocess, sys, math
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde

work_DIR = sys.argv[1]
freqFile = sys.argv[2]
LDFile = sys.argv[3]
os.chdir(work_DIR)

freq = np.genfromtxt(freqFile)
freq = freq[1:len(freq)]

LD = np.genfromtxt(LDFile, delimiter='\t', skip_header=1)

############# TO BE CONTINUED ... 2017 07 24