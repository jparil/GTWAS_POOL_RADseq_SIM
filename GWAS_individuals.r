#!/usr/bin/Rscript
#miscellaneous script---superceeded by "basisPlots_GWAS_ROC.py"!!!!!!!!
#UPDATED: superceeded the superceeder!!! 2017 08 25
args = commandArgs(trailingOnly=TRUE)
workDIR = args[1]
phenFile = args[2]
genoFile = args[3]
phenMod = args[4]

setwd(workDIR)

geno=read.delim(genoFile, sep="\t", header=T)
phen=read.delim(phenFile, sep="\t", header=F)

X = t(geno)
y = as.matrix(phen[,as.numeric(phenMod)])

#GLM
p.val = c()
for (i in 1:ncol(X))
{
	test = try(summary(lm(y ~ X[,i]))$coef[2,4], silent=TRUE)
	if(inherits(test, "try-error"))
	{
		p.val = c(p.val, 1)
	}
	else
	{
		p.val = c(p.val, summary(lm(y ~ X[,i]))$coef[2,4])
	}
}

LOD = -log(p.val, base=10)

jpeg("ALL_LOCI Manhattan Plot - GLM.jpeg", width=900, height=500, quality=100)
plot(0,pch='',xlim=c(0,ncol(X)),xlab="Chromosome",xaxt='n',ylab=expression(-log[10](italic(p))),ylim=c(0,max(LOD,na.rm=T)),bty="n",las=2,main="Man with a hat hat hat!")
points(LOD)
dev.off()

write.table(LOD, file="GWAS_GLM_LOD.temp", sep=",", row.names=F, col.names=F)

#for testing
# phenFile = "Simulated_Lolium_perenne_PHENOTYPES.data"
# genoFile = "Simulated_Lolium_perenne_GENOTYPES.numeric"
# phenMod = 1