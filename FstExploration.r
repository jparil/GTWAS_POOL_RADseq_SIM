#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)

#Fst analysis
# Fst ==> measure of population structure
#		Fst = (Ht - Hs) / Ht; where Ht = He(E(p)) and Hs = E(He(p) across subpop)
#		| Fst=0 means - subpopulations are perfect representives of the whole population!
#		| Fst=1 means - means extreme population structure - subpopulations are completely different from one another!

#INPUT:
#	(1) genotype file in sync format ALONE! Woah with this we need not phenotypic data! --> where not looking for causal SNPs related to phenotypes but just for SNPs which show high variability across pools

#OUTPUT:
#	(a) manhattan plot where x==> loci and y== Fst

library(stringr)
dat = read.delim(args[1], header=F)

NPOOLS = ncol(dat) - 3
NLOCI = nrow(dat)

for (p in seq(NPOOLS)) {
	print(p)
	COUNTS = str_split_fixed(dat[,p+3], ":", 6)
	COUNTS =  as.data.frame(apply(COUNTS, 2, as.numeric))
	FREQ = COUNTS/rowSums(COUNTS)
	EXHET = 1 - rowSums(FREQ^2)
	assign(paste("FREQ", p, sep="_"), FREQ)
	assign(paste("EXHET", p, sep="_"), EXHET)
}


FREQ_POP = matrix(0, nrow=NLOCI, ncol=6)
EXHET_SUB = matrix(0, nrow=NLOCI, ncol=1)
for (p in seq(NPOOLS)) {
	FREQ_POP = FREQ_POP + eval(parse(text=paste("FREQ", p, sep="_")))
	EXHET_SUB = EXHET_SUB + eval(parse(text=paste("EXHET", p, sep="_")))
}
FREQ_POP = FREQ_POP / NPOOLS
EXHET_POP = 1 - rowSums(FREQ_POP^2)
EXHET_SUB = EXHET_SUB / NPOOLS

Fst = (EXHET_POP - EXHET_SUB) / EXHET_POP

plot(c(1:NLOCI), Fst)