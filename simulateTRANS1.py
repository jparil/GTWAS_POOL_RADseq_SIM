#!/usr/bin/env python
#generate the numeric transcript data
import os, subprocess, sys, math
import numpy as np
from Bio import SeqIO
from Bio import Seq
from Bio.SeqRecord import SeqRecord

workDIR = sys.argv[1]
transcript = sys.argv[2]
nIND = int(sys.argv[3])
name = sys.argv[4]

#maybe there are better ways to define these:
readLength_MIN = 75
readLength_MAX = 200
readStart_ZIG = 0.10

os.chdir(workDIR)

#(1) reading the base or reference transcriptome:
with open(transcript) as handler:
	rna_sequences = list(SeqIO.FastaIO.SimpleFastaParser(handler))

nTRANS = len(SeqIO.index(transcript,'fasta'))

#(2) simulating trascript absolute abundance: (do i need to connect these transcript with them QTL at this stage???!!!! NOPE! The phenotype model we'll use here is simply additive! At first at least ;-P)
absolute_BASE = np.ceil(np.random.beta(5, 5, size=nTRANS)*100).astype('int')
absolute_GENO = np.ceil(np.random.chisquare(0.01, size=(nIND, nTRANS))*100).astype('int')
absolute_GENO = np.nan_to_num(absolute_GENO)
absolute_ABUNDANCE = absolute_BASE + absolute_GENO
np.savetxt("Simulated_Lolium_perenne_TRANSCRIPT_BASE.data", absolute_BASE, fmt="%i", delimiter="\t")
np.savetxt("Simulated_Lolium_perenne_TRANSCRIPT_GENO.data", absolute_GENO, fmt="%i", delimiter="\t")