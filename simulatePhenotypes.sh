#!/bin/bash
DIR_DNA=$1
DIR_PHE=$(paste -d'/' <(echo $1 | rev | cut -d'/' -f2- | rev) <(echo PHE))
DIR_RNA=$(paste -d'/' <(echo $1 | rev | cut -d'/' -f2- | rev) <(echo RNA))
GENFILE=$2
NQTL=$3
H2=$(bc -l <<< ${4}/100)
NPOOL=$5
MODEL=$6
RNA_BASE=$7
RNA_GENO=$8

#for testing:
# VOLUME=/mnt/SIMULATED/DNA
# GENFILE=Simulated_Lolium_perenne_GENOTYPES.data
# NQTL=10
# H2=0.50
# NPOOL=5
# MODEL=1
# RNA_BASE=
# RNA_GENO_=

cd $DIR_DNA

#extract binary genotype data prior assigning phenotypic values
cut -d$'\t' -f3- $GENFILE > numeric.temp

#######################
##					 ##
## ASSIGN PHENOTYPES ##
##					 ##
#######################
if [ $MODEL == 1 ]
then
~/GTWAS_POOL_RADseq_SIM/assignPheno.py $DIR_DNA numeric.temp $NQTL $H2 1
elif [ $MODEL == 2 ]
then
~/GTWAS_POOL_RADseq_SIM/assignPheno.py $DIR_DNA numeric.temp $NQTL $H2 2 ${DIR_RNA}/${RNA_BASE} ${DIR_RNA}/${RNA_GENO}
fi
	#ARGUMENTS:
	#	(1) working directory
	#	(2) numeric genotype file - without the scaffold and position columns
	#	(3) number of QTLs to simulate
	#	(4) heritability in decimal form
	#	(5) phenotype model to use
	#OUPUTS:
	#	(1) Simulated_Lolium_perenne_PHENOTYPES.data ---> headerless phenotypic values
	#	(2) Simulated_Lolium_perenne_QTL.temp ---> QTL locations: absolute index and effect, headerless

cd $DIR_DNA
for i in $(seq $(cat QTL.temp | wc -l))
do
index=$(head -n${i} QTL.temp | tail -n1 | cut -d$'\t' -f1)
head -n$((${index%%.*}+2)) Genotypes.data| tail -n1 | cut -d$'\t' -f1,2 >> QTL.pos.temp
done
echo -e 'SCAFFOLD\tPOS\tINDEX\tQTL_EFFECTS' > QTL.data
paste -d'\t' QTL.pos.temp QTL.temp >> QTL.data
rm *.temp

#####################
##				   ##
## POOL PHENOTYPES ##
##				   ##
#####################
~/GTWAS_POOL_RADseq_SIM/poolingPheno.py $DIR_DNA Phenotypes.data $NPOOL
	#ARGUMENTS:
	#	(1) working directory
	#	(2) the raw phenotypic data or output 2 of assignPheno.py
	#	(3) number of pools
	#OUTPUTS:
	#	(1) Simulated_Lolium_perenne_PHENOTYPES_POOLED.index --> the pooling of the phenotypes and the corresponding index for pooling of the fasta files
	#	(2) Simulated_Lolium_perenne_PHENOTYPES.py --> the input phenotype data in python format for GWAlpha
