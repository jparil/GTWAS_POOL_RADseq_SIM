#!/usr/bin/env python
import os, subprocess, sys, math
import numpy as np

work_DIR = sys.argv[1]
fileName = sys.argv[2]
os.chdir(work_DIR)

PHEN = np.genfromtxt(fileName, delimiter='\t')
#PHEN = ALL_PHEN[:,
pMIN = np.amin(PHEN)
pMAX = np.amax(PHEN)
pMEAN = np.mean(PHEN)
pVAR = np.var(PHEN)

labels = range(1, len(PHEN)+1)
P = np.column_stack((labels, PHEN))
P = P[np.argsort(P[:, 1])]

#percentiles=np.array((0.064, 0.256, 0.744,0.936)) #why these? 0.64 - 0.192 - 0.488 - 0.192 - 0.064 differences
percentiles=np.array((0.2, 0.4, 0.6, 0.8))
q = np.percentile(P[:,1], percentiles*100)

POOL=[]
for i in P[:,1]:
	if(i < q[0]):
		POOL.append(1)
	elif(i < q[1]):
		POOL.append(2)
	elif(i < q[2]):
		POOL.append(3)
	elif(i < q[3]):
		POOL.append(4)
	else:
		POOL.append(5)

POOLS_OUT = np.column_stack((POOL, P))
np.savetxt("Simulated_Lolium_perenne_PHENOTYPES_POOLED.txt", POOLS_OUT, fmt="%s", delimiter="\t")
os.system("echo 'POOL\tINDEX\tPHENO' > Simulated_Lolium_perenne_PHENOTYPES_POOLED.index; cat Simulated_Lolium_perenne_PHENOTYPES_POOLED.txt >> Simulated_Lolium_perenne_PHENOTYPES_POOLED.index; rm Simulated_Lolium_perenne_PHENOTYPES_POOLED.txt")

pyPhen_OUT = open("Simulated_Lolium_perenne_PHENOTYPES.py", "w")
pyPhen_OUT.write("Pheno_name = 'Sim_Lperenne';\nsig=" + str(pVAR) + ";\nMIN=" + str(pMIN) + ";\nMAX=" + str(pMAX) + ";\nperc=[" + str(percentiles[0]) + "," + str(percentiles[1]) + "," + str(percentiles[2]) + "," + str(percentiles[3]) + "];\nq=[" + str(q[0]) + "," + str(q[1]) + "," + str(q[2]) + "," + str(q[3]) + "];")
pyPhen_OUT.close()
