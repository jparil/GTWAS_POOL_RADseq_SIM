#!/usr/bin/env python
import os, subprocess, sys, math, random
import numpy as np
from Bio import SeqIO
from Bio import Seq

work_DIR = sys.argv[1]
nLOCI = int(sys.argv[2])
varPerScaf = int(sys.argv[3])
nIND = int(sys.argv[4])
genPREFIX = sys.argv[5]
variant_VCF = sys.argv[6]
output_VCF = sys.argv[7]
os.chdir(work_DIR)

#(3->4) SIMULATE BINARY GENOTYPE DATA:
#freqREF = np.random.random(nLOCI).reshape((nLOCI, 1))	#ref allele freq sampled from uniform distribution between 0 and 1 --> OR SHOULD I USE A BETA DISTRIBUTION HERE?
freqREF = np.random.beta(2, 2, nLOCI).reshape((nLOCI, 1))
freqALT = np.ones((nLOCI, 1)) - freqREF

#######################
#	WITHOUT LINKAGE
#######################
genID=[]
for n in range(nIND):
	gen = np.random.binomial(2, freqREF.reshape((1, nLOCI)).tolist()[0], size=nLOCI)
	GEN=[]
	for g in gen:
		if g == 2:
			GEN.append("0/0")
		elif g == 1:
			GEN.append("0/1")
		else:
			GEN.append("1/1")
	GEN = np.array(GEN).reshape((nLOCI, 1))
	try:
		GEN_OUT = np.column_stack((GEN_OUT, GEN))
	except:
		GEN_OUT = GEN
	genID.append(genPREFIX + str(n+1))

genID = np.array(genID).reshape((1, nIND))
GEN_OUT = np.concatenate((genID, GEN_OUT), axis=0)

# spitting out genotypes simulated without LD:
np.savetxt("binGEN.txt", GEN_OUT, fmt='%s' ,delimiter="\t")
os.system("echo ' ' > newline")
os.system("cat newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline binGEN.txt > binGEN.vcf")
os.system("paste -d'\t' " + variant_VCF + " binGEN.vcf > " + "Simulated_Lolium_perenne-without_LD.vcf")
os.system("rm newline")
os.system("rm binGEN*")
#spitting out reference allele frequencies:
np.savetxt("freqs.txt", freqREF, fmt='%s' ,delimiter="\t")
os.system("grep 'scaffold' Simulated_Lolium_perenne_VARIANT.vcf > freqs2.txt")
os.system("cut -d '\t' -f1 freqs2.txt > freqs3.txt")
os.system("paste freqs3.txt freqs.txt > Simulated_Lolium_perenne_GENOTYPES.freq")
os.system("rm freqs*")
#######################           SOMETHING TERRIBLY WRONG HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 2017 07 26
#	WITH LINKAGE: ---> same LD (r^2) for all adjacent loci within scaffolds
#######################
# Pab_max = min(Pa, Pb)/max(Pa, Pb)
# r2_max = (Pab - PaPb)^2 / PaPb(1-Pa)(1-Pb)
genID=[]
phase=np.random.choice([0,1], size=nLOCI)
for n in range(nIND):
	GEN=[]
	for i in range(nLOCI):
		if (i+1)%varPerScaf==1:
			a1 = np.random.choice([0, 1], p=(freqREF[i][0], freqALT[i][0]))	#No LD between scaffolds
			a2 = np.random.choice([0, 1], p=(freqREF[i][0], freqALT[i][0]))
		else:
			if phase[i-1]==phase[i]:
				if (phase[i-1]==0 and a1==0) or (phase[i-1]==1 and a1==0):
					Pab = min(freqREF[i-1], freqREF[i]) / max(freqREF[i-1], freqREF[i])
					a1 = np.random.choice([0, 1], p=(Pab[0], 1-Pab[0]))
					a2 = np.random.choice([0, 1], p=(Pab[0], 1-Pab[0]))
				else:
					Pab = min(freqALT[i-1], freqALT[i]) / max(freqALT[i-1], freqALT[i])
					a1 = np.random.choice([1, 0], p=(Pab[0], 1-Pab[0]))
					a2 = np.random.choice([1, 0], p=(Pab[0], 1-Pab[0]))
			else:
				if (phase[i-1]==0 and a1==0) or (phase[i-1]==1 and a1==0):
					Pab = min(freqREF[i-1], freqALT[i]) / max(freqREF[i-1], freqALT[i])
					a1 = np.random.choice([1, 0], p=(Pab[0], 1-Pab[0]))
					a2 = np.random.choice([1, 0], p=(Pab[0], 1-Pab[0]))
				else:
					Pab = min(freqALT[i-1], freqREF[i]) / max(freqALT[i-1], freqREF[i])
					a1 = np.random.choice([0, 1], p=(Pab[0], 1-Pab[0]))
					a2 = np.random.choice([0, 1], p=(Pab[0], 1-Pab[0]))
		GEN.append(str(a1) + "|" + str(a2))
	GEN = np.array(GEN).reshape((nLOCI, 1))
	try:
		GEN_OUT = np.column_stack((GEN_OUT, GEN))
	except:
		GEN_OUT = GEN
	genID.append(genPREFIX + str(n+1))

genID = np.array(genID).reshape((1, nIND))
GEN_OUT = np.concatenate((genID, GEN_OUT), axis=0)

#	FINALLY add genotype columns including column names
np.savetxt("binGEN.txt", GEN_OUT, fmt='%s' ,delimiter="\t")
os.system("echo ' ' > newline")
os.system("cat newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline binGEN.txt > binGEN.vcf")
os.system("paste -d'\t' " + variant_VCF + " binGEN.vcf > " + output_VCF)
os.system("rm newline")
os.system("rm binGEN*")
###:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::###
####################### Memmory error in generating the large covariance matrix
#	ALTERNATIVE LINKAGE MODEL WITH MULTIVARIATE NORMAL DISTRIBUTION
####################### PLUS random sampling from the multivariate normal distribution takes too long!
# def generateCovarianceMatrix(nLOCI):
# 	COV = np.random.uniform(0,1,size=(nLOCI, nLOCI))
# 	COV = (COV + COV.T)/2
# 	row_sums = COV.sum(axis=1)
# 	COV = COV / row_sums[:, np.newaxis]
# 	return COV

# for scaffold in range(nLOCI/varPerScaf):
# 	COV = generateCovarianceMatrix(varPerScaf)
# 	X = np.random.multivariate_normal(mean=freqALT[scaffold*varPerScaf:(scaffold*varPerScaf)+varPerScaf,0],cov=COV,size=nIND*2)
# 	X[X<.5]=0
# 	X[X>=.5]=1
# 	try:
# 		gen = np.column_stack((gen, X))
# 	except:
# 		gen = X

# gen=gen.T
# genID=[]
# for n in range(nIND):
# 	genID.append(genPREFIX + str(n+1))
# 	GEN=[]
# 	for i in range(nLOCI):
# 		GEN.append(str(int(gen[i, (n*2)])) + "|" + str(int(gen[i, (n*2)+1])))
# 	try:
# 		GEN_OUT = np.column_stack((GEN_OUT, GEN))
# 	except:
# 		GEN_OUT = GEN

# genID = np.array(genID).reshape((1, nIND))
# GEN_OUT = np.concatenate((genID, GEN_OUT), axis=0)

# #	FINALLY add genotype columns including column names
# np.savetxt("binGEN.txt", GEN_OUT, fmt='%s' ,delimiter="\t")
# os.system("echo ' ' > newline")
# os.system("cat newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline binGEN.txt > binGEN.vcf")
# os.system("paste -d'\t' " + variant_VCF + " binGEN.vcf > " + "Simulated_Lolium_perenne-alternative_LD_MODEL.vcf")
# os.system("rm newline")
# os.system("rm binGEN*")
# ###:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::###
