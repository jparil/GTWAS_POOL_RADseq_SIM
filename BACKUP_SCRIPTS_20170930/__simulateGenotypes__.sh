#!/bin/bash
#		(1) directory where Lolium_perenne_genome_FIXED.fasta is located
#		(2) number of variants per scaffold to simulate
#		(3) number of genotypes to simulate
#		(4) prefix of the genotype names
#		(5)	LD stat?
DIR=$1
NVAR=$2
NGEN=$3
PREFIX=$4
LD=$5

#for testing
# DIR=/${VOLUME}/SIMULATED/DNA
# NVAR=20
# NGEN=500
# PREFIX=GENO
# LD=N

#(1) Simulate one vcf file with all the possible variants
~/SCRIPTS/__simulateVAR__.py $DIR \
Lolium_perenne_genome_FIXED.fasta \
Simulated_Lolium_perenne_VARIANT.fasta \
Simulated_Lolium_perenne_VARIANT.vcf \
$NVAR

#(2) Simulate a number of genotypes and stack them all together in one vcf file
cd $DIR
nLOCI=$(grep "scaffold" ${1}/Simulated_Lolium_perenne_VARIANT.vcf | wc -l)

~/SCRIPTS/simulateGEN__V2a__.py $DIR \
${nLOCI} \
$NVAR \
$NGEN \
$PREFIX \
Simulated_Lolium_perenne_VARIANT.vcf

parallel ~/SCRIPTS/simulateGEN__V2b__.py $DIR phase.txt $NVAR Simulated_Lolium_perenne_GENOTYPES.freq ${PREFIX}{1} ::: $(seq $NGEN)

#(3) Finishing up
#concatenate genotypes
cd $1
paste -d$'\t' GENO* >> Simulated_Lolium_perenne_GENOTYPES.vcf.temp
echo ' ' > newline
cat newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline Simulated_Lolium_perenne_GENOTYPES.vcf.temp > binGEN.temp
paste -d$'\t' Simulated_Lolium_perenne_VARIANT.vcf binGEN.temp > Simulated_Lolium_perenne_GENOTYPES.vcf
rm GENO* *.temp newline phase.txt
#extract basic vcf file statistics including LD in R^2
if [ $LD == Y ]
then
vcftools --vcf Simulated_Lolium_perenne_GENOTYPES.vcf
vcftools --vcf Simulated_Lolium_perenne_GENOTYPES.vcf --freq --out Simulated_Lolium_perenne_GENOTYPES.vcfTOOLS
if [ $nLOCI -gt 10000 ]
then
head -n10000 Simulated_Lolium_perenne_GENOTYPES.vcf > forLD_extract.vcf
else
head -n$((19 + $nLOCI)) Simulated_Lolium_perenne_GENOTYPES.vcf > forLD_extract.vcf
fi
vcftools --vcf forLD_extract.vcf --hap-r2 --out Simulated_Lolium_perenne_GENOTYPES_PHASED.vcfTOOLS
cut -d$'\t' -f5 Simulated_Lolium_perenne_GENOTYPES.vcfTOOLS.frq |cut -d: -f2 > allele.freq
#plotting LD and allele frequencies
~/SCRIPTS/plotLDandAlleleFreq.R $1 allele.freq Simulated_Lolium_perenne_GENOTYPES_PHASED.vcfTOOLS.hap.ld
rm allele.freq forLD_extract.vcf
fi
#extracting numeric data
grep "scaffold" Simulated_Lolium_perenne_GENOTYPES.vcf | cut -d$'\t' -f1,2,4,5 > Simulated_Lolium_perenne_LOCI.temp
grep "scaffold" Simulated_Lolium_perenne_GENOTYPES.vcf | cut -d$'\t' -f1,2 > geno.loci
grep "scaffold" Simulated_Lolium_perenne_GENOTYPES.vcf | cut -d$'\t' -f10- > geno.data
paste -d$'\t' geno.loci geno.data > geno.txt
sed 's/0|0/2/g' geno.txt | sed 's/1|1/0/g' | sed 's/1|0/1/g' | sed 's/0|1/1/g' >> geno.num
#extracting headers or genotype names
grep "#CHROM" Simulated_Lolium_perenne_GENOTYPES.vcf | cut -d$'\t' -f8- > header.data
sed 's/INFO/LOCI/g' header.data | sed 's/FORMAT/POS/g'> header.txt
#pineapple - apple uhhmmmm!
cat header.txt geno.num > Simulated_Lolium_perenne_GENOTYPES.data
#extract binary genotype data prior assigning phenotypic values
#cut -d$'\t' -f3- Simulated_Lolium_perenne_GENOTYPES.data > Simulated_Lolium_perenne_GENOTYPES.numeric
#summarize loci data
cut -d$'\t' -f2 Simulated_Lolium_perenne_GENOTYPES.freq | paste -d$'\t' Simulated_Lolium_perenne_LOCI.temp - > Simulated_Lolium_perenne_LOCI.data
rm Simulated_Lolium_perenne_GENOTYPES.freq Simulated_Lolium_perenne_LOCI.temp
#clean up
rm geno*
rm header*
#gzipping the vcf file of all genotypes
# cd $DIR
# bgzip -c Simulated_Lolium_perenne_GENOTYPES.vcf > Simulated_Lolium_perenne_GENOTYPES.vcf.gz
# tabix -p vcf Simulated_Lolium_perenne_GENOTYPES.vcf.gz
