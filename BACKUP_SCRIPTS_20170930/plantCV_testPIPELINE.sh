#!/bin/bash

mkdir OUTPUT/

/home/ubuntu/SCRIPTS/plantCV_testSeparatePLANTS.py \
-i /home/ubuntu/SCRIPTS/plantCV_test/testAthaliana.jpg \
-o /home/ubuntu/SCRIPTS/plantCV_test/OUTPUT \
-n plantCV_testLabels.txt \
-D None

cd /home/ubuntu/SCRIPTS/plantCV_test/OUTPUT/
touch LEAF_AREA.txt
ls *jpg > plants_JPEG.list
for i in $(ls *jpg)
do
/home/ubuntu/SCRIPTS/plantCV_testExtractAREA.py ${i}
cat plantCV.out >> LEAF_AREA.txt
rm plantCV.out
echo ${i}
done