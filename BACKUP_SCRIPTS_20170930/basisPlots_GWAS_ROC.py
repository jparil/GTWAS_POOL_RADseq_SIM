#!/usr/bin/env python
import os, subprocess, sys, math
import pandas
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from collections import Counter
from sklearn import metrics
#from statsmodels.formula.api import ols
import scipy

workDIR = sys.argv[1]
phenotypeFile = sys.argv[2]
genotypeFile = sys.argv[3]
qtlFile = sys.argv[4]
phenModel = int(sys.argv[5]) - 1
os.chdir(workDIR)

PHEN = np.genfromtxt(phenotypeFile, delimiter='\t')
GENO = np.genfromtxt(genotypeFile, delimiter='\t', skip_header=1)
QTL = np.genfromtxt(qtlFile, delimiter='\t', skip_header=1)

nLOCI = GENO.shape[0]
nIND = GENO.shape[1]

LOCI_FX= np.zeros((nLOCI, 1), dtype=float)
LOCI_FX[QTL[:,2].astype(int), 0] = QTL[:,3]

#Observed phenotypes (y=Xb+e) vs simulated phenotypes (y=Xb)
y_OBS = PHEN[:,phenModel].reshape((nIND,1))
y_SIM = np.matmul(GENO.T, LOCI_FX)
COR = np.corrcoef(y_OBS[:,0],y_SIM[:,0])[0,1]
OBSvsSIM = plt.figure()
OBSvsSIM.suptitle("Observed x Simulated Phenotypes")
plt.xlabel("Observed Phenotypic Values")
plt.ylabel("Simulated Phenotypic Values")
plt.scatter(y_OBS, y_SIM, label="r = %0.2f" % COR)
plt.legend(loc="lower right")
plt.savefig("Observed x Simulated Phenotypes Scatterplot.png")
#plt.show()

#TOO SL#OW!!!! will be replacing the GLM GWAS with an R script! 2017 08 25
#GWAS wi#th raw vcf capturing all QTL ----->>>>>>>>>>>>>>>>>>>>..#superceeded by GWAS_individuals.R!!! 2017 08 25
pValues=[]
y = PHEN[:,phenModel]
for i in range(nLOCI):
	x = GENO[i,:]
	slope, intercept, rvalue, pvalue, stderr = scipy.stats.linregress(x, y)
	pValues.append(pvalue)

os.system("tail -n$(grep 'scaffold' Simulated_Lolium_perenne_VARIANT.vcf | wc -l) Simulated_Lolium_perenne_VARIANT.vcf | cut -d'\t' -f1,2 > scaffNames.txt")
scaffNames = np.genfromtxt("scaffNames.txt", delimiter="\t", usecols=0, dtype='string')
locations = np.genfromtxt("scaffNames.txt", delimiter="\t", usecols=1, dtype='int')
os.system("rm scaffNames.txt")

DATAFRAME = pandas.DataFrame({'Scaffold': scaffNames, 'Location': locations, 'p_values': pValues, 'LOD': -np.log10(pValues), 'QTL_effects': LOCI_FX[:,0]})

#LOD x QTL effects
SCATT = plt.figure()
SCATT.suptitle("LOD x Actual QTL effects (GLM all variant loci captured)")
plt.xlabel("LOD (-log10(p-value))")
plt.ylabel("QTL Effects")
plt.scatter(DATAFRAME.LOD, DATAFRAME.QTL_effects)
plt.savefig("ALL_LOCI LOD x Actual QTL Effects - GLM.png")
#plt.show()

#Manhattan plot
SCAFF_GROUPING = np.repeat(range(nLOCI), repeats=nLOCI/len(Counter(DATAFRAME['Scaffold'])))
MANHAT = plt.figure()
MANHAT.suptitle("Manhattan Plot (unpooled GWAS all variant loci captured)")
plt.xlabel("Scaffolds")
plt.ylabel("LOD")
for i in range(nLOCI):
	plt.plot((SCAFF_GROUPING[i]*i)+i, DATAFRAME.LOD[i], ls='', marker='.')

plt.savefig("ALL_LOCI Manhattan Plot - GLM.png")
#plt.show()

#ROC (Receiver operating characteristics) plot
LOCI_binFX = np.zeros((nLOCI,1), dtype='int')
LOCI_binFX[QTL[:,2].astype(int), 0] = 1
y = LOCI_binFX[:,0]
scores = 1-DATAFRAME.p_values
fpr, tpr, thresholds = metrics.roc_curve(y, scores)
auc = metrics.auc(fpr, tpr)
ROC = plt.figure()
ROC.suptitle("ROC-GLM (unpooled GWAS all variant loci captured)")
plt.xlabel("P(False Positives) or 1-Specificity")
plt.ylabel("P(True Positives) or Sensitivity")
#plt.xlim([0.00, 1.00])
#plt.ylim([0.00, 1.05])
plt.plot(fpr,tpr, color='darkred', label='AUC = %0.2f' % auc)
plt.legend(loc='lower right')
plt.plot([0,1],[0,1], color='gray', linestyle='--')
plt.savefig("ALL_LOCI ROC - GLM.png")
#plt.show()

#for testing
# workDIR = "/mnt/SIMULATED/DNA"
# phenotypeFile = "Simulated_Lolium_perenne_PHENOTYPES.data"
# genotypeFile = "Simulated_Lolium_perenne_GENOTYPES.numeric"
# qtlFile = "Simulated_Lolium_perenne_QTL.data"
# phenModel = 0