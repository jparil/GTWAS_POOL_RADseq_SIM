#!/usr/bin/env python
import os, subprocess, sys, math
import pandas
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from collections import Counter
from sklearn import metrics

# #for testing:
# workDIR = "/mnt/SIMULATED/test/DNA/"
# GWAS_OUT = "GWAlpha_${rep}-${npool}POOLS-${nqtl}QTL-${h2}H2-${ENZYME1}x${ENZYME2}_out.csv" 
# QTL = "Simulated_Lolium_perenne_QTL.data"
# threshold_distance=1000

workDIR = sys.argv[1]
GWAS_OUT = sys.argv[2]
QTL = sys.argv[3]
threshold_distance = int(sys.argv[4])
os.chdir(workDIR)

scaffolds = np.genfromtxt(GWAS_OUT, delimiter=',', skip_header=1, usecols=0, dtype='string')
positions = np.genfromtxt(GWAS_OUT, delimiter=',', skip_header=1, usecols=1, dtype='int')
alphas = np.absolute(np.genfromtxt(GWAS_OUT, delimiter=',', skip_header=1, usecols=3, dtype='float'))
GWAlpha_DATAFRAME = pandas.DataFrame({'scaffold':scaffolds, 'positions':positions, 'alphas':alphas})

QTL_scaffolds = np.genfromtxt(QTL, delimiter='\t', skip_header=1, usecols=0, dtype='string')
QTL_positions = np.genfromtxt(QTL, delimiter='\t', skip_header=1, usecols=1, dtype='int')
QTL_DATAFRAME = pandas.DataFrame({'QTL_scaffold':QTL_scaffolds, 'QTL_position':QTL_positions})

# #sort SNPs by p-values
# GWAlpha_DATAFRAME = GWAlpha_DATAFRAME.sort_values('alphas')

# y = np.zeros((scaffolds.shape[0],), dtype='int')
# nSNP = GWAlpha_DATAFRAME.shape[0]
# nQTL = QTL_DATAFRAME.shape[0]
# for i in range(nSNP):
# 	for j in range(nQTL):
# 		if abs(alphas[i]) > 0.02:
# 			if scaffolds[i] == QTL_scaffolds[j]:
# 				if abs(positions[i]-QTL_positions[j]):
# 					y[i] = 1

y = np.zeros((scaffolds.shape[0],), dtype='int')
for i in range(len(QTL_positions)):
	testSCAF = np.in1d(scaffolds, QTL_scaffolds[i])
	testPOS = np.in1d(positions[testSCAF], QTL_positions[i])
	if sum(testPOS) == 1:
		y[testSCAF] = y[testSCAF] + testPOS.astype(int)
	elif sum(testPOS)>0:
		POSITIONS = positions[testSCAF]
		DISTANCES = []
		y_temp=np.zeros((len(POSITIONS),), dtype='int')
		for k in range(len(POSITIONS)):
			DISTANCES.append(abs(POSITIONS[k] - QTL_positions[i]))
		#if DISTANCES[np.argmin(DISTANCES)] < threshold_distance:
		y_temp[np.argmin(DISTANCES)] = 1
		y[testSCAF] = y[testSCAF] + y_temp
		

DATAFRAME = pandas.DataFrame({'Scaffold': scaffolds, 'Location': positions, 'Alpha': alphas, 'Alpha^2': alphas**2, 'Detected_Correctly': y})

#ROC (Receiver operating characteristics) plot
scores = np.absolute(alphas)
scores[scores<0] = 0	#I'm getting p-values greater than 1! - setting them as arbitrarily high as LOD = 0
fpr, tpr, thresholds = metrics.roc_curve(y, scores)
auc = metrics.auc(fpr, tpr)
ROC = plt.figure()
ROC.suptitle("ROC Plot")
plt.xlabel("P(False Positives) or 1-Specificity")
plt.ylabel("P(True Positives) or Sensitivity")
#plt.xlim([0.00, 1.00])
#plt.ylim([0.00, 1.05])
plt.plot(fpr,tpr, color='darkred', label='AUC = %0.2f' % auc)
plt.legend(loc='lower right')
plt.plot([0,1],[0,1], color='gray', linestyle='--')
plt.savefig("ROC.png")
#plt.show()

#for testing
# workDIR = "/mnt/SIMULATED/DNA"
# GWAS_OUT = "GWAlpha_sim-ALL_LOCI_out.csv"
# QTL = "Simulated_Lolium_perenne_QTL.data"
# threshold_distance = 1000

#for testing
# workDIR = "/mnt/SIMULATED/DNA/EcoRIxMseI"
# GWAS_OUT = "GWAlpha_sim-EcoRIxMseI_out.csv"
# QTL = "../Simulated_Lolium_perenne_QTL.data"
# threshold_distance = 1000