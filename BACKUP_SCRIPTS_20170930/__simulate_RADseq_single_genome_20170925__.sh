#!/bin/bash
VOLUME=$1
MINFRAG=$2
MAXFRAG=$3
INSLEN=$4
MUTPROB=$5
LOCNUM=$6
READNUM=$7
PREFIX=$8
KEEP_DEPTH=$9

#for testing:
# VOLUME=volume1
# MINFRAG=100
# MAXFRAG=800
# FRAGINT=25
# INSLEN=150
# MUTPROB=0.80
# LOCNUM=200000
# READNUM=10000000
# PREFIX=TEST_INIT
# enzyme1=EcoRI
# enzyme2=MseI

mem=$(($(($(grep MemTotal /proc/meminfo | awk '{print $2}') / 1000000))-5))
thr=$(($(grep -c ^processor /proc/cpuinfo)-1))

cd ~/SOFTWARES/ddRADseqTools/Package*/
for k in $(seq $(cat Renzyme.txt | wc -l))
do
enzyme1=$(head -n$k Renzyme.txt | tail -n 1)

for j in $(seq $((k+1)) $(cat Renzyme.txt | wc -l))
do
enzyme2=$(head -n$j Renzyme.txt | tail -n 1)

#simulate RE digetion
./rsitesearch.py \
--genfile=/${VOLUME}/SIMULATED/DNA/Lolium_perenne_genome_FIXED.fasta \
--enzyme1=$enzyme1 --enzyme2=$enzyme2 \
--minfragsize=$MINFRAG --maxfragsize=$MAXFRAG \
--fragstfile=/${VOLUME}/SIMULATED/DNA/${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-REDIGEST.txt \
--fragsfile=/${VOLUME}/SIMULATED/DNA/${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-FRAGMENTS.fasta &> /dev/null

#simulate sequencing
./simddradseq.py \
--fragsfile=/${VOLUME}/SIMULATED/DNA/${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-FRAGMENTS.fasta \
--individualsfile=poolsIND.txt \
--technique=IND1_IND2_DBR \
--enzyme1=$enzyme1 --enzyme2=$enzyme2 \
--readtype=PE \
--mutprob=$MUTPROB \
--insertlen=$INSLEN \
--locinum=$LOCNUM \
--readsnum=$READNUM \
--format=FASTQ \
--readsfile=/${VOLUME}/SIMULATED/DNA/${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-ILLREADS &> /dev/null
rm /${VOLUME}/SIMULATED/DNA/${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-FRAGMENTS*

#variant calling with bcftools mpileup .. | bcftools call ..
cd /${VOLUME}/SIMULATED/DNA/
#align, compress, and  sort
bwa mem -M /${VOLUME}/SIMULATED/DNA/Lolium_perenne_genome \
${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-ILLREADS-1.fastq \
${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-ILLREADS-2.fastq | \
samtools view -Sb | samtools sort > ${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-SORTED.bam
rm ${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-ILLREADS-*

#extract coverage from sorted bam file
samtools depth -aa ${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-SORTED.bam \
> ${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-DEPTH.csv
rm ${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-SORTED.bam

#summarize
TOT_BASES=$(wc -l ${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-DEPTH.csv | cut -d$' ' -f1)
TOT_READS=$(awk '{ total += $3 } END {print total}' ${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-DEPTH.csv)
TOTSQ_READS=$(awk '{ total += $3^2 } END {printf "%.0f", total}' ${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-DEPTH.csv)
if [ $KEEP_DEPTH == 0 ]
then
rm ${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-DEPTH.csv ${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-REDIGEST*
fi
rm ${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-REDIGEST.txt

#OUTPUT TO SUMMARY FILE ${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-OUT.txt
# <REP> 	<MIN_FRAG> 	<MAX_FRAG> 	<ENZYME_1> 	<ENZYME_2> 	<TOTAL_BASES> <TOTAL_READS> <SUM_SQUARED_READS>
echo -e "$PREFIX\t$enzyme1\t$enzyme2\t$MINFRAG\t$MAXFRAG\t$INSLEN\t$READNUM\t$TOT_BASES\t$TOT_READS\t$TOTSQ_READS" \
> /${VOLUME}/SIMULATED/DNA/${PREFIX}-${enzyme1}x${enzyme2}-${MINFRAG}-${MAXFRAG}-${INSLEN}-${READNUM}-OUT.txt

#clean up
cd ~/SOFTWARES/ddRADseqTools/Package*/
done
done