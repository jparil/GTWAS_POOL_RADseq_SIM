#RESET

#for newNECTAR and sim03
rm ~/SCRIPTS/*.*
rm -R /mnt/SIMULATED/DNA
rm -R /mnt/SIMULATED/RNA
mkdir /mnt/SIMULATED/DNA
mkdir /mnt/SIMULATED/RNA

#for sim02 --> with them attached 2TB volume :-D
rm ~/SCRIPTS/*.*
rm -R /volume1/SIMULATED/DNA
rm -R /volume1/SIMULATED/RNA
mkdir /volume1/SIMULATED/DNA
mkdir /volume1/SIMULATED/RNA