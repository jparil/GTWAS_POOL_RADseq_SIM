#!/bin/bash
cd ~/SCRIPTS
if [ $4 == 1 ]
then
./assignPheno.py /${1}/SIMULATED/DNA Simulated_Lolium_perenne_GENOTYPES.numeric $2 $3 1
elif [ $4 == 2 ]
then
./assignPheno.py /${1}/SIMULATED/DNA Simulated_Lolium_perenne_GENOTYPES.numeric $2 $3 2 /${1}/SIMULATED/RNA/Simulated_Lolium_perenne_TRANSCRIPT_BASE.data /${1}/SIMULATED/RNA/Simulated_Lolium_perenne_TRANSCRIPT_GENO.data
fi
	#ARGUMENTS:
	#	(1) working directory
	#	(2) numeric genotype file - without the scaffold and position columns
	#	(3) number of QTLs to simulate
	#	(4) heritability in decimal form
	#	(5) phenotype model to use
	#	(6) transcriptome file 1: base levels -- use if model > 1
	#	(7) transcriptome file 1: genotype-specific levels --- use if model > 1
	#OUTPUTS:
	#	(1) Simulated_Lolium_perenne_QTL.data ---> the locations (scaffold and positions) and effects of simulated QTL
	#	(2) Simulated_Lolium_perenne_QTL.out
cd /${1}/SIMULATED/DNA
for i in $(seq $(cat Simulated_Lolium_perenne_QTL.out | wc -l))
do
index=$(head -n${i} Simulated_Lolium_perenne_QTL.out | tail -n1 | cut -d$'\t' -f1)
head -n$((${index%%.*}+2)) Simulated_Lolium_perenne_GENOTYPES.data| tail -n1 | cut -d$'\t' -f1,2 >> QTL.pos
done
echo SCAFFOLD'\t'POS'\t'INDEX'\t'QTL_EFFECTS > Simulated_Lolium_perenne_QTL.data
paste -d'\t' QTL.pos Simulated_Lolium_perenne_QTL.out >> Simulated_Lolium_perenne_QTL.data
rm Simulated_Lolium_perenne_QTL.out
rm QTL.pos
cd ~/SCRIPTS
	#	(3) Simulated_Lolium_perenne_PHENOTYPES.data --> the raw phenotypic data

#for testing 2017 08 09
# work_DIR = "/mnt/SIMULATED/DNA"
# genoFile = "Simulated_Lolium_perenne_GENOTYPES.numeric"
# nQTL = 10
# heritability = 0.50
# transBase = "/mnt/SIMULATED/RNA/Simulated_Lolium_perenne_TRANSCRIPT_BASE.data"
# transGeno = "/mnt/SIMULATED/RNA/Simulated_Lolium_perenne_TRANSCRIPT_GENO.data"

#sort phenotypes and divide into 5 pools
#inverse-quantile normalize the phenotypic values
#spit out the index of the individuals corresponding to the pools they belong to
#spit out the python phenotype file for GWAlpha
./poolingPheno.py /${1}/SIMULATED/DNA Simulated_Lolium_perenne_PHENOTYPES.data
	#ARGUMENTS:
	#	(1) working directory
	#	(2) the raw phenotypic data or output 2 of assignPheno.py
	#OUTPUTS:
	#	(1) Simulated_Lolium_perenne_PHENOTYPES_POOLED.index --> the pooling of the phenotypes and the corresponding index for pooling of the fasta files
	#	(2) Simulated_Lolium_perenne_PHENOTYPES.py --> the input phenotype data in python format for GWAlpha
