#!/usr/bin/env python
import os, subprocess, sys, math
import numpy as np

# #for testing:
# DIR = "/mnt/SIMULATED/DNA"
# fileName_RADseq = "R_INPUT_RADseq.temp"
# fileName_GENpool = "R_INPUT_GENpool.temp"
# fileName_LOCIdata = "R_INPUT_LOCIdata.temp"

DIR = sys.argv[1]
fileName_RADseq = sys.argv[2]
fileName_GENpool = sys.argv[3]
fileName_LOCIdata = sys.argv[4]

os.chdir(DIR)

SCAF_R = np.atleast_1d(np.genfromtxt(fileName_RADseq, delimiter='\t', usecols=0, dtype='str'))
SCAF_G = np.atleast_1d(np.genfromtxt(fileName_GENpool, delimiter='\t', usecols=0, dtype='str'))
#SCAF_L = np.genfromtxt(fileName_LOCIdata, delimiter='\t', usecols=0, dtype='str')

POS_R = np.atleast_1d(np.genfromtxt(fileName_RADseq, delimiter='\t', usecols=1, dtype='int'))
POS_G = np.atleast_1d(np.genfromtxt(fileName_GENpool, delimiter='\t', usecols=1, dtype='int'))
#POS_L = np.genfromtxt(fileName_LOCIdata, delimiter='\t', usecols=1, dtype='int')

COVERAGE = np.atleast_1d(np.genfromtxt(fileName_RADseq, delimiter='\t', usecols=2, dtype='int'))
freq_temp = np.atleast_1d(np.genfromtxt(fileName_GENpool, delimiter='\t', dtype='float'))
FREQ = freq_temp[:,2:]
REF = np.genfromtxt(fileName_LOCIdata, delimiter='\t', usecols=2, dtype='str')
ALT = np.genfromtxt(fileName_LOCIdata, delimiter='\t', usecols=3, dtype='str')

NPOOL = FREQ.shape[1]
NPOS_G = len(POS_G)
NPOS_R = len(POS_R)

for pool in range(NPOOL):
	out2=[]
	out_scaf=[]
	out_pos=[]
	out_ref=[]
	for i in range(NPOS_R):
		for j in range(NPOS_G):
			A=np.array([0]); T=np.array([0]); C=np.array([0]); G=np.array([0]); N=np.array([0]); DEL=np.array([0])
			if (POS_R[i] == POS_G[j]):
				if (REF[j] == "A"):
					A = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					if (ALT[j] == "T"):
						T = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					elif (ALT[j] == "C"):
						C = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					elif (ALT[j] == "G"):
						G = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					elif (ALT[j] == "N"):
						N = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					else:
						DEL = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
				elif (REF[j] == "T"):
					T = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					if (ALT[j] == "A"):
						A = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					elif (ALT[j] == "C"):
						C = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					elif (ALT[j] == "G"):
						G = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					elif (ALT[j] == "N"):
						N = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					else:
						DEL = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
				elif (REF[j] == "C"):
					C = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					if (ALT[j] == "A"):
						A = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					elif (ALT[j] == "T"):
						T = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					elif (ALT[j] == "G"):
						G = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					elif (ALT[j] == "N"):
						N = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					else:
						DEL = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)	
				elif (REF[j] == "G"):
					G = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					if (ALT[j] == "A"):
						A = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					elif (ALT[j] == "T"):
						T = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					elif (ALT[j] == "C"):
						C = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					elif (ALT[j] == "N"):
						N = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					else:
						DEL = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
				elif (REF[j] == "N"):
					N = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					if (ALT[j] == "A"):
						A = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					elif (ALT[j] == "T"):
						T = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					elif (ALT[j] == "C"):
						C = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					elif (ALT[j] == "G"):
						G = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					else:
						DEL = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
				else:
					DEL = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					if (ALT[j] == "A"):
						A = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					elif (ALT[j] == "T"):
						T = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					elif (ALT[j] == "C"):
						C = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					elif (ALT[j] == "G"):
						G = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
					else:
						N = np.random.binomial(n=COVERAGE[i], p=FREQ[j, pool], size=1)
				out1 = str(A[0]) + ":" + str(T[0]) + ":" + str(C[0]) + ":" + str(G[0]) + ":" + str(N[0]) + ":" + str(DEL[0])
				out_scaf.append(SCAF_G[j])
				out_pos.append(POS_G[j])
				out_ref.append(REF[j])
		out2.append(out1)
	try:
		out3 = np.column_stack((out3, out2))
	except:
		out3 = out2

OUT = np.column_stack((out_scaf, out_pos, out_ref, out3))


np.savetxt("SYNC.temp", OUT, fmt="%s", delimiter="\t")