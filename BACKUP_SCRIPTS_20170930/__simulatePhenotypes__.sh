#!/bin/bash
VOLUME=$1
GENFILE=$2
NQTL=$3
H2=$(bc -l <<< ${4}/100)
NPOOL=$5
MODEL=$6
RNA_BASE=$7
RNA_GENO=$8

#for testing:
# VOLUME=mnt
# GENFILE=Simulated_Lolium_perenne_GENOTYPES.data
# NQTL=10
# H2=0.50
# NPOOL=5
# MODEL=1
# RNA_BASE=
# RNA_GENO_=

cd /${VOLUME}/SIMULATED/DNA

#extract binary genotype data prior assigning phenotypic values
cut -d$'\t' -f3- $GENFILE > numeric.temp

#######################
##					 ##
## ASSIGN PHENOTYPES ##
##					 ##
#######################
if [ $MODEL == 1 ]
then
~/SCRIPTS/__assignPheno__.py /${VOLUME}/SIMULATED/DNA numeric.temp $NQTL $H2 1
elif [ $MODEL == 2 ]
then
~/SCRIPTS/assignPheno.py /${VOLUME}/SIMULATED/DNA numeric.temp $NQTL $H2 2 /${VOLUME}/SIMULATED/RNA/${RNA_BASE} /${VOLUME}/SIMULATED/RNA/${RNA_GENO}
fi
	#ARGUMENTS:
	#	(1) working directory
	#	(2) numeric genotype file - without the scaffold and position columns
	#	(3) number of QTLs to simulate
	#	(4) heritability in decimal form
	#	(5) phenotype model to use
	#OUPUTS:
	#	(1) Simulated_Lolium_perenne_PHENOTYPES.data ---> headerless phenotypic values
	#	(2) Simulated_Lolium_perenne_QTL.temp ---> QTL locations: absolute index and effect, headerless

cd /${VOLUME}/SIMULATED/DNA
for i in $(seq $(cat Simulated_Lolium_perenne_QTL.temp | wc -l))
do
index=$(head -n${i} Simulated_Lolium_perenne_QTL.temp | tail -n1 | cut -d$'\t' -f1)
head -n$((${index%%.*}+2)) Simulated_Lolium_perenne_GENOTYPES.data| tail -n1 | cut -d$'\t' -f1,2 >> QTL.pos.temp
done
echo -e 'SCAFFOLD\tPOS\tINDEX\tQTL_EFFECTS' > Simulated_Lolium_perenne_QTL.data
paste -d'\t' QTL.pos.temp Simulated_Lolium_perenne_QTL.temp >> Simulated_Lolium_perenne_QTL.data
rm *.temp

#####################
##				   ##
## POOL PHENOTYPES ##
##				   ##
#####################
~/SCRIPTS/__poolingPheno__.py /${VOLUME}/SIMULATED/DNA Simulated_Lolium_perenne_PHENOTYPES.data $NPOOL
	#ARGUMENTS:
	#	(1) working directory
	#	(2) the raw phenotypic data or output 2 of assignPheno.py
	#	(3) number of pools
	#OUTPUTS:
	#	(1) Simulated_Lolium_perenne_PHENOTYPES_POOLED.index --> the pooling of the phenotypes and the corresponding index for pooling of the fasta files
	#	(2) Simulated_Lolium_perenne_PHENOTYPES.py --> the input phenotype data in python format for GWAlpha
