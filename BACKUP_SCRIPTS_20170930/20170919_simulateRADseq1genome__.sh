#!/bin/bash
MINFRAG=$1
INCFRAG=$2
INSLEN=$3
REP=$4
VOLUME=$5
FRAGINT=$6
MUTPROB=$7
LOCNUM=$8
READNUM=$9
NPOOL=${10}

#for testing
# MINFRAG=100
# INCFRAG=50
# INSLEN=100
# REP=99
# VOLUME=mnt
# FRAGINT=25
# MUTPROB=0.80
# LOCNUM=200000
# READNUM=12000000
# NPOOL=5
# enzyme1=EcoRI
# enzyme2=MseI

mem=$(($(($(grep MemTotal /proc/meminfo | awk '{print $2}') / 1000000))-5))
thr=$(($(grep -c ^processor /proc/cpuinfo)-1))
mkdir /${VOLUME}/SIMULATED/DNA_r${REP}
# cp /${VOLUME}/SIMULATED/DNA/Lolium_perenne_genome* /${VOLUME}/SIMULATED/DNA_r${REP}/
# cp /${VOLUME}/SIMULATED/DNA/INPUT.fasta /${VOLUME}/SIMULATED/DNA_r${REP}/

cd ~/SOFTWARES/ddRADseqTools/Package*/
for k in $(seq $(cat Renzyme.txt | wc -l))
do
enzyme1=$(head -n$k Renzyme.txt | tail -n 1)

for j in $(seq $((k+1)) $(cat Renzyme.txt | wc -l))
do
enzyme2=$(head -n$j Renzyme.txt | tail -n 1)

#simulate RE digetion
./rsitesearch.py \
--genfile=/${VOLUME}/SIMULATED/DNA/Lolium_perenne_genome_FIXED.fasta \
--enzyme1=$enzyme1 --enzyme2=$enzyme2 \
--minfragsize=$MINFRAG --maxfragsize=$(($MINFRAG+$INCFRAG)) \
--fragstinterval=$FRAGINT \
--fragstfile=/${VOLUME}/SIMULATED/DNA_r${REP}/Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-REDIGEST.txt \
--fragsfile=/${VOLUME}/SIMULATED/DNA_r${REP}/Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-FRAGMENTS.fasta &> /dev/null
#duplicate fragments fasta
mv /${VOLUME}/SIMULATED/DNA_r${REP}/Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-FRAGMENTS.fasta /${VOLUME}/SIMULATED/DNA_r${REP}/Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-FRAGMENTS.temp.fasta
touch /${VOLUME}/SIMULATED/DNA_r${REP}/Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-FRAGMENTS.fasta
for CAT in $(seq 101)
do
cat /${VOLUME}/SIMULATED/DNA_r${REP}/Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-FRAGMENTS.temp.fasta >> /${VOLUME}/SIMULATED/DNA_r${REP}/Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-FRAGMENTS.fasta
echo $CAT
done
#simulate sequencing
./simddradseq.py \
--fragsfile=/${VOLUME}/SIMULATED/DNA_r${REP}/Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-FRAGMENTS.fasta \
--individualsfile=poolsIND.txt \
--technique=IND1_IND2_DBR \
--enzyme1=$enzyme1 --enzyme2=$enzyme2 \
--readtype=PE \
--mutprob=$MUTPROB \
--insertlen=$INSLEN \
--locinum=$LOCNUM \
--readsnum=$READNUM \
--format=FASTQ \
--readsfile=/${VOLUME}/SIMULATED/DNA_r${REP}/Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-ILLREADS &> /dev/null
#rm /${VOLUME}/SIMULATED/DNA_r${REP}/Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-FRAGMENTS*
#rm /${VOLUME}/SIMULATED/DNA_r${REP}/Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-REDIGEST*
#demultiplexing
./indsdemultiplexing.py \
--technique=IND1_IND2_DBR \
--format=FASTQ \
--individualsfile=poolsIND.txt \
--readsfile1=/${VOLUME}/SIMULATED/DNA_r${REP}/Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-ILLREADS-1.fastq \
--readsfile2=/${VOLUME}/SIMULATED/DNA_r${REP}/Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-ILLREADS-2.fastq &> /dev/null
#rm /${VOLUME}/SIMULATED/DNA_r${REP}/Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-ILLREADS-*
#trimming
for pool in $(seq $NPOOL)
do
./readstrim.py \
--technique=IND1_IND2_DBR \
--format=FASTQ \
--readtype=PE \
--readsfile1=/${VOLUME}/SIMULATED/DNA_r${REP}/demultiplexed-POOL${pool}-1.fastq \
--readsfile2=/${VOLUME}/SIMULATED/DNA_r${REP}/demultiplexed-POOL${pool}-2.fastq \
--trimfile=/${VOLUME}/SIMULATED/DNA_r${REP}/Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-POOL${pool}-TRIMMED &> /dev/null
#variant calling with bcftools mpileup .. | bcftools call ..
cd /${VOLUME}/SIMULATED/DNA_r${REP}
#align them bunch of illumina reads into the reference genome built above
bwa mem -t $thr -M /${VOLUME}/SIMULATED/DNA/Lolium_perenne_genome \
Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-POOL${pool}-TRIMMED-1.fastq \
Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-POOL${pool}-TRIMMED-2.fastq \
> Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-POOL${pool}-ALIGNED.sam
#rm Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-POOL${pool}-TRIMMED-*.fastq
#compress the aligned sam file into a bam file
samtools view -@ $thr -Sb \
Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-POOL${pool}-ALIGNED.sam \
> Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-POOL${pool}-ALIGNED.bam
#sort these bam files for indexing
samtools sort -@ $thr -m ${mem}G Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-POOL${pool}-ALIGNED.bam \
> Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-POOL${pool}-ALIGNED.sorted
mv Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-POOL${pool}-ALIGNED.sorted Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-POOL${pool}-ALIGNED.bam
#build bam index, #remove duplicates and #rebuild cleaned bam index with picard
java -ea -Xmx${mem}g -jar ~/SOFTWARES/picard.jar BuildBamIndex \
I=Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-POOL${pool}-ALIGNED.bam
#rm *.sam
cd ~/SOFTWARES/ddRADseqTools/Package*/
done
#rm /${VOLUME}/SIMULATED/DNA_r${REP}/demultiplexed*

#variant calling without duplicate removal
cd /${VOLUME}/SIMULATED/DNA_r${REP}
ls Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-POOL*-ALIGNED.bam > bam.list
samtools mpileup -b bam.list \
> Simulated_Lolium_perenne-${enzyme1}x${enzyme2}.mpileup
java -ea -Xmx${mem}g -jar ~/SOFTWARES/popoolation2*/mpileup2sync.jar \
--input Simulated_Lolium_perenne-${enzyme1}x${enzyme2}.mpileup \
--output Simulated_Lolium_perenne-${enzyme1}x${enzyme2}.sync.temp \
--fastq-type sanger \
--min-qual 30

# removing monomorphic loci (TEST 20170904 - awk:::alex's suggestion! Works perfectly! :-D)
awk -v OFS='\t' '{if ($4!=$5 || $5!=$6 || $6!=$7 || $7!=$8) print $1,$2,$3,$4,$5,$6,$7,$8}' Simulated_Lolium_perenne-${enzyme1}x${enzyme2}.sync.temp > Simulated_Lolium_perenne-${enzyme1}x${enzyme2}.sync

#INSERT ASSESSMENT STEPS HERE:
echo ${enzyme1}x${enzyme2} > Simulated_Lolium_perenne_RADseq_${enzyme1}x${enzyme2}_COVERAGE.csv
grep "scaffold" Simulated_Lolium_perenne-${enzyme1}x${enzyme2}.sync | cut -f1 -d$'\t' > SCAF.list.temp
uniq SCAF.list.temp > SCAF.names.temp
uniq -c SCAF.list.temp | sed 's/ s/:s/g' | grep -n scaffold | cut -f2 -d: | sed 's/ //g' > SCAF.counts.temp
paste -d, SCAF.names.temp SCAF.counts.temp >> Simulated_Lolium_perenne_RADseq_${enzyme1}x${enzyme2}_COVERAGE.csv
#extract sequencing coverage with samtools
#samtools depth -a Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-POOL*-ALIGNED.bam > Simulated_Lolium_perenne_RADseq_DEPTH.csv
samtools depth Simulated_Lolium_perenne-${enzyme1}x${enzyme2}-POOL*-ALIGNED.bam > Simulated_Lolium_perenne_RADseq_DEPTH.csv
NDEPTH=$(wc -l Simulated_Lolium_perenne_RADseq_DEPTH.csv | cut -d$' ' -f1)
TOTREADS=$(awk '{ total += $3 } END {print total}' Simulated_Lolium_perenne_RADseq_DEPTH.csv)
#SQREADS=$(awk '{ total += $3^2 } END {printf "%.0f", total}' Simulated_Lolium_perenne_RADseq_DEPTH.csv)
UNIQREADS=$(wc -l Simulated_Lolium_perenne_RADseq_DEPTH.csv | cut -d$' ' -f1)
if [ $NDEPTH == 0 ]
then
aveDEPTH=0
varDEPTH=0
else
aveDEPTH=$(($TOTREADS / $(($NDEPTH * 1))))
#varDEPTH=$(($(($SQREADS / $(($NDEPTH * 1)))) - $(($aveDEPTH*$aveDEPTH))))
#echo $aveDEPTH > ~/aveDEPTH_${REP}_${MINFRAG}_${INCFRAG}_${INSLEN}_${enzyme1}_${enzyme2}_.out
#echo $NDEPTH > ~/NDEPTH_${REP}_${MINFRAG}_${INCFRAG}_${INSLEN}_${enzyme1}_${enzyme2}_.out
fi

#OUTPUT TO SUMMARY FILE sim_${REP}_${1}_${2}_${3}_${enzyme1}_${enzyme2}_.out --then concatenate everybody into per insert length--- ~/RADseq_SIMULATION.out
# <REP> 	<MIN_FRAG> 	<MAX_FRAG> 	<ENZYME_1> 	<ENZYME_2> 	<N_SCAFF> 	<TOTAL_SNP> <AVE_DEPTH> <VAR_DEPTH>
touch ~/sim_${REP}_${MINFRAG}_${INCFRAG}_${INSLEN}_${enzyme1}_${enzyme2}_.out
#echo -e "$REP\t$MINFRAG\t$(($MINFRAG+$INCFRAG))\t$INSLEN\t$enzyme1\t$enzyme2\t$(cat SCAF.names.temp | wc -l)\t$(paste -sd+ SCAF.counts.temp | bc)\t$aveDEPTH\t$varDEPTH" >> ~/sim_${REP}_${MINFRAG}_${INCFRAG}_${INSLEN}_${enzyme1}_${enzyme2}_.out
echo -e "$REP\t$MINFRAG\t$(($MINFRAG+$INCFRAG))\t$INSLEN\t$enzyme1\t$enzyme2\t$(cat SCAF.names.temp | wc -l)\t$(paste -sd+ SCAF.counts.temp | bc)\t$aveDEPTH\t$UNIQREADS" >> ~/sim_${REP}_${MINFRAG}_${INCFRAG}_${INSLEN}_${enzyme1}_${enzyme2}_.out

#clean up
#rm bam.list *.temp *${enzyme1}x${enzyme2}*.*
cd ~/SOFTWARES/ddRADseqTools/Package*/
done
done

rm -R /${VOLUME}/SIMULATED/DNA_r${REP}
