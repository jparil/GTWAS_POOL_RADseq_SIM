#!/usr/bin/env python
import os, subprocess, sys, math
import numpy as np
from Bio import SeqIO
from Bio import Seq
from Bio.SeqRecord import SeqRecord

workDIR = sys.argv[1]
transcript = sys.argv[2]
nIND = int(sys.argv[3])
name = sys.argv[4]
absolute_BASE_fn = sys.argv[5]
absolute_GENO_fn = sys.argv[6]
indID = int(sys.argv[7])

#maybe there are better ways to define these:
readLength_MIN = 75
readLength_MAX = 200
readStart_ZIG = 0.10

os.chdir(workDIR)

#(1) reading the base or reference transcriptome:
with open(transcript) as handler:
	rna_sequences = list(SeqIO.FastaIO.SimpleFastaParser(handler))

nTRANS = len(SeqIO.index(transcript,'fasta'))

#(2) open the numeric trascript abundance data from simulateTRANS1.py
absolute_BASE = np.genfromtxt(absolute_BASE_fn, delimiter='\t', dtype=int)
absolute_GENO = np.genfromtxt(absolute_GENO_fn, delimiter='\t', dtype=int)
absolute_ABUNDANCE = absolute_BASE + absolute_GENO

#(3) translating these abundance data into the actual fasta files with stochasticity in read frames and read lengths
NEW_FASTA = []
r=-1
for RNA in rna_sequences:
	r = r+1
	for k in range(absolute_ABUNDANCE[indID-1, r]):
		LEN = len(RNA[1])
		readLength = np.random.choice((range(readLength_MIN, readLength_MAX)))
		readStart = np.random.choice(range(int(math.ceil(LEN*readStart_ZIG))))
		if LEN < (readStart+readLength):
			SEQUENCE = SeqRecord(RNA[1][readStart:LEN], id="read_" + str(indID) + "_" + str(r) + "_" + str(k))
		else:
			SEQUENCE = SeqRecord(RNA[1][readStart:(readStart+readLength)], id="read_" + str(indID) + "_" + str(r) + "_" + str(k))
		SEQUENCE.letter_annotations["phred_quality"] = np.random.choice(range(38,42), size=len(SEQUENCE))
		NEW_FASTA.append(SEQUENCE)
SeqIO.write(NEW_FASTA, "Simulated_Lolium_perenne_TRANSCRIPT_" + name + str(indID) + ".fastq", "fastq")
