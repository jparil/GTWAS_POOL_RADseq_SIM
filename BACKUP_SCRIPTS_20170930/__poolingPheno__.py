#!/usr/bin/env python
import os, subprocess, sys, math
import numpy as np

#for testing
work_DIR="/mnt/SIMULATED/DNA"
fileName="Simulated_Lolium_perenne_PHENOTYPES.data"
nPools=5

work_DIR = sys.argv[1]
fileName = sys.argv[2]
nPools = int(sys.argv[3])
os.chdir(work_DIR)

PHEN = np.genfromtxt(fileName, delimiter='\t')
pMIN = np.amin(PHEN)
pMAX = np.amax(PHEN)
pMEAN = np.mean(PHEN)
pVAR = np.var(PHEN)

labels = range(1, len(PHEN)+1)
P = np.column_stack((labels, PHEN))
P = P[np.argsort(P[:, 1])]

percentiles = []
for i in range(1, nPools):
	percentiles.append(float(i)/nPools)

percentiles = np.array(percentiles)
q = np.percentile(P[:,1], percentiles*100)

POOL=[]
for i in P[:,1]:
	for j in range(nPools-1):
		if(i < q[j]):
			POOL.append(j+1)
			break
		elif(i > q[nPools-2]):
			POOL.append(nPools)
			break

POOLS_OUT = np.column_stack((POOL, P))
np.savetxt("Simulated_Lolium_perenne_PHENOTYPES_POOLED.txt", POOLS_OUT, fmt="%s", delimiter="\t")
os.system("echo 'POOL\tINDEX\tPHENO' > Simulated_Lolium_perenne_PHENOTYPES_POOLED.index; cat Simulated_Lolium_perenne_PHENOTYPES_POOLED.txt >> Simulated_Lolium_perenne_PHENOTYPES_POOLED.index; rm Simulated_Lolium_perenne_PHENOTYPES_POOLED.txt")

pyPhen_OUT = open("Simulated_Lolium_perenne_PHENOTYPES.py", "w")
pyPhen_OUT.write("Pheno_name = 'Sim_Lperenne';\nsig=" + str(pVAR) + ";\nMIN=" + str(pMIN) + ";\nMAX=" + str(pMAX) + ";\nperc=[" + str(percentiles[0]))
for i in range(1, nPools-1):
	pyPhen_OUT.write("," + str(percentiles[i]))
pyPhen_OUT.write("];\nq=[" + str(q[0]))
for i in range(1, nPools-1):
	pyPhen_OUT.write("," + str(q[i]))
pyPhen_OUT.write("];")
pyPhen_OUT.close()
