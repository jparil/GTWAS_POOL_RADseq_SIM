#!/usr/bin/env python
#--->>> simulateGEN__V2a__.py
# testing for gnu parallel
import os, subprocess, sys, math, random
import numpy as np
from Bio import SeqIO
from Bio import Seq

work_DIR = sys.argv[1]
nLOCI = int(sys.argv[2])
varPerScaf = int(sys.argv[3])
nIND = int(sys.argv[4])
genPREFIX = sys.argv[5]
variant_VCF = sys.argv[6]
os.chdir(work_DIR)

#(3->4) SIMULATE BINARY GENOTYPE DATA:
#freqREF = np.random.random(nLOCI).reshape((nLOCI, 1))	#ref allele freq sampled from uniform distribution between 0 and 1 --> OR SHOULD I USE A BETA DISTRIBUTION HERE?
freqREF = np.random.beta(2, 2, nLOCI).reshape((nLOCI, 1))
freqALT = np.ones((nLOCI, 1)) - freqREF

# #######################
# #	GENERATE GENOTYPES WITHOUT LINKAGE - no need to parallelize this - its quick af ;-P
# #######################
# genID=[]
# for n in range(nIND):
# 	gen = np.random.binomial(2, freqREF.reshape((1, nLOCI)).tolist()[0], size=nLOCI)
# 	GEN=[]
# 	for g in gen:
# 		if g == 2:
# 			GEN.append("0/0")
# 		elif g == 1:
# 			GEN.append("0/1")
# 		else:
# 			GEN.append("1/1")
# 	GEN = np.array(GEN).reshape((nLOCI, 1))
# 	try:
# 		GEN_OUT = np.column_stack((GEN_OUT, GEN))
# 	except:
# 		GEN_OUT = GEN
# 	genID.append(genPREFIX + str(n+1))

# genID = np.array(genID).reshape((1, nIND))
# GEN_OUT = np.concatenate((genID, GEN_OUT), axis=0)

# # spitting out genotypes simulated without LD:
# np.savetxt("binGEN.txt", GEN_OUT, fmt='%s' ,delimiter="\t")
# os.system("echo ' ' > newline")
# os.system("cat newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline newline binGEN.txt > binGEN.vcf")
# os.system("paste -d'\t' " + variant_VCF + " binGEN.vcf > " + "Simulated_Lolium_perenne-without_LD.vcf")
# os.system("rm newline")
# os.system("rm binGEN*")
#spitting out reference allele frequencies:
np.savetxt("freqs.txt", freqREF, fmt='%s' ,delimiter="\t")
os.system("grep 'scaffold' Simulated_Lolium_perenne_VARIANT.vcf > freqs2.txt")
os.system("cut -d '\t' -f1 freqs2.txt > freqs3.txt")
os.system("paste freqs3.txt freqs.txt > Simulated_Lolium_perenne_GENOTYPES.freq")
os.system("rm freqs*")
#######################
#	GENERATE THE LINKAGE STATES - actual genotype data will be generated in parallel by simulateGEN__V2b__.py
#######################
phase=np.random.choice([0,1], size=nLOCI)
np.savetxt("phase.txt", phase, fmt='%s' ,delimiter="\t")

#for testing:
# work_DIR = "/mnt/SIMULATED/DNA"
# nLOCI = 280
# varPerScaf = 20
# nIND = 500
# genPREFIX = "GENO"
# variant_VCF = "Simulated_Lolium_perenne_VARIANT.vcf"