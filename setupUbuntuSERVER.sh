#!/bin/bash

#wait a minute or two before ssh-ing into the new Nectar instances. It takes about a couple of minutes to set-up the ip address. Don't worry especially if know you've set up the ssh secutiry group and keys.

# #for persistent volume storage:
# sudo mkdir /volume1
# sudo mount /dev/vdc /volume1 -t auto

VOLUME=mnt

# Oy! Oy! Oy! The Australian repository may be down. Do $:
cd /etc/apt/sources.list.d/
sudo mv au_archive_ubuntu_com_ubuntu.list us_archive_ubuntu_com_ubuntu.list
sudo sed -i 's/au./us./g' us_archive_ubuntu_com_ubuntu.list
sudo apt update

cd ~
mkdir SOFTWARES
mkdir BLAST
mkdir SEQUENCES
mkdir SEQUENCES/DNA
mkdir SEQUENCES/RNA
mkdir SEQUENCES/PRO
mkdir SCRIPTS

sudo chown ubuntu /mnt

cd /${VOLUME}
mkdir SIMULATED
mkdir SIMULATED/DNA
mkdir SIMULATED/RNA
mkdir SIMULATED/PRO
mkdir SIMULATED/PHE

#(02)########## set up softwares
sudo apt install -y make libncurses5-dev libncursesw5-dev python3-dev python-dev libbz2-dev liblzma-dev python3-numpy python-numpy unzip git htop default-jre build-essential zlib1g-dev bwa vcftools r-base python-pip python3-pip python-tk parallel libboost-all-dev bedtools bc
pip install numpy scipy matplotlib Biopython statsmodels pandas sklearn pymp-pypi pysam; pip install pysamstats; pip install opencv-python scikit-image pyvcf h5py

homeDIR=/home/ubuntu
cd ${homeDIR}/SOFTWARES
wget http://zzz.bwh.harvard.edu/plink/dist/plink-1.07-x86_64.zip
wget https://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/current/sratoolkit.current-ubuntu64.tar.gz
wget -O ddRADseqTools.zip https://github.com/GGFHF/ddRADseqTools/archive/master.zip
git clone git://github.com/samtools/samtools.git
git clone git://github.com/samtools/htslib.git
git clone git://github.com/samtools/bcftools.git
wget https://github.com/broadinstitute/picard/releases/download/2.10.0/picard.jar
#git clone https://github.com/zstephens/neat-genreads.git
wget -O popoolation2.zip https://sourceforge.net/projects/popoolation2/files/latest/download
#scp -i ${homeDIR}/.ssh/KEY ubuntu@130.220.208.152://home/ubuntu/SOFTWARES/GenomeAnalysisTK-3.7-0.tar.bz2 . #### MODIFY ME!!!
git clone https://github.com/aflevel/GWAlpha.git
wget -O cufflinks.tar.gz http://cole-trapnell-lab.github.io/cufflinks/assets/downloads/cufflinks-2.2.1.Linux_x86_64.tar.gz
git clone https://github.com/alexdobin/STAR.git
wget https://github.com/COMBINE-lab/salmon/releases/download/v0.9.1/Salmon-0.9.1_linux_x86_64.tar.gz #probably modify this link if a new version comes out

unzip plink*.zip
tar -vxzf sratoolkit.current-ubuntu64.tar.gz
unzip ddRADseqTools.zip
unzip popoolation2.zip
#tar xvjf GenomeAnalysisTK*.tar.bz2
tar xvzf cufflinks*.tar.gz
rm -R *.tar*
rm -R *.zip*

chmod +x GWAlpha/*.py 
chmod +x GWAlpha/*.r
mv sratoolkit* sratoolkit
mv ddRADseqTools-master ddRADseqTools
mv ddRADseqTools/Package* ddRADseqTools/Package
chmod +x ddRADseqTools/Package/*.py
cd samtools
make
sudo make install
cd ../htslib
make
sudo make install
cd ../bcftools
make
sudo make install

export PATH=$PATH:${homeDIR}/SOFTWARES/sratoolkit/bin

cd ~
git clone https://gitlab.com/jeffersonfparil/GTWAS_POOL_RADseq_SIM.git
git clone https://github.com/aflevel/GWAlpha.git
chmod +x GWAlpha/*.py GWAlpha/*.r GWAlpha/*.sh
cp -R SOFTWARES/ddRADseqTools .

#(03)##################### pull the reference genome
cd ${homeDIR}/SEQUENCES/DNA/
wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/001/735/685/GCA_001735685.1_ASM173568v1/GCA_001735685.1_ASM173568v1_genomic.fna.gz
wget http://185.45.23.197:5080/ryegrassdata/GENOMIC_ASSEMBLY/lope_V1.0.fasta
gzip -d  GCA_001735685.1_ASM173568v1_genomic.fna.gz
# wget ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/001/735/GCF_000001735.3_TAIR10/GCF_000001735.3_TAIR10_genomic.fna.gz #Arabidopsis thaliana genome: Assembly GCA_000001735.1	Date: 2001/08/13
# gzip -d GCF_000001735.3_TAIR10_genomic.fna.gz
rm -R *.gz

cd ${homeDIR}/SEQUENCES/RNA/
wget http://185.45.23.197:5080/ryegrassdata/GENOMIC_ASSEMBLY/lope_transcripts.V1.0.fasta	#predicted transcripts from the above genome: tissue and stress specific transcriptome can be found in another directory
wget http://185.45.23.197:5080/ryegrassdata/GENE_ANNOTATIONS/Lp_Annotation_V1.1.mp.gff3		#gene annotations

cd ${homeDIR}/SEQUENCES/PRO/
wget http://185.45.23.197:5080/ryegrassdata/GENOMIC_ASSEMBLY/lope_proteins.V1.0.fasta
